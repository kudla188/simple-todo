$(document).ready(function(){
	// Initialize tooltips
		$('[data-toggle="tooltip"]').tooltip();
	// Initialize nette ajax
		$.nette.init();

	// Initialize confirm modals
	$(document).on("click", '[data-confirm-modal]', function(e) {

		// Disable default event
		e.preventDefault();

		// Get modal
		var modal = $($(this).data("confirm-modal"));

		// Get link from anchor
		var link = $(this).attr("href");

	 // Get message
		var message = $(this).data("confirm-message");

		// Apply it to modal
		modal.find(".modal-message").text(message);

		// Find modal and save it for future use
		var submitButton = modal.find(".modal-submit");

		// Apply link to modal's submit button
		submitButton.attr("href", link);

		// Assign click to hide a modal
		submitButton.on("click", function() {
			modal.modal("hide");
		});

		// Show it
		modal.modal("show");
	});

});
