<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;

class HomepagePresenter extends Nette\Application\UI\Presenter
{
  /**
    * @var \Nette\Database\Context Database context instance
    */
  private $db;

  /**
    * @var int Id of currently edited task
    */
  private $editId = null;

  /**
    * @var string State of new tasks
    */
  const STATE_NEW = "New";
  /**
    * @var string State of done tasks
    */
  const STATE_DONE = "Done";

  /**
    * @var string Name of column that contains task state
    */
  const COLUMN_STATE = "state";

  /**
    * @var string Name of column that contains task name
    */
  const COLUMN_NAME = "name";

  /**
    * @var string Name of column that contains task text
    */
  const COLUMN_TEXT = "text";

  /**
    * @var string Name of table
    */
  const TABLE_TASKS = "tasks";

  public function __construct(\Nette\Database\Context $db)
  {
      // Assign injected context into presenter global var
      $this->db = $db;
  }

  /**
    * AddTask form factory
    */
  public function createComponentAddTask() {
    // If editid isn't null form is in edit mode
    $isEditing = $this->editId != null;

    // Create form
    $form = new UI\Form();

    // Add textbox for name + add validation
    $form->addText(self::COLUMN_NAME, "Name")
          ->setRequired(TRUE);

    // Add textarea for text + add validation
    $form->addTextArea(self::COLUMN_TEXT, "Text")
          ->setRequired(TRUE);

    // If form is in edit mode
    if($isEditing) {
      // Add hidden input that contains id
      $form->addHidden("editid", $this->editId);

      // Set default values
      $form->setDefaults($this->db->table(self::TABLE_TASKS)->wherePrimary($this->editId)->fetch());
    } else {
      // Still add edit id, because of some nette security mechanism
      $form->addHidden("editid");
    }

    // Add submit button
    $form->addSubmit("submit", $isEditing ? "Save" : "Add");

    // Assign handling method to success event
    $form->onSuccess[] = [$this, "addTaskSucceeded"];

    // Redraw form
    $this->redrawControl("form");

    // Cross site forgery
    $form->addProtection('Time for sending form expired, please try again.');

    // Return created form
    return $form;
  }

  /**
    * Add method that handles sending of form
    */
  public function addTaskSucceeded(UI\Form $form) {
    // Get values entered into form
    $values = $form->getValues();

    // If is found task with same id use edit mode
    if(isset($values->editid) && $this->db->table(self::TABLE_TASKS)->wherePrimary($values->editid)->count() > 0) {
      // Store edit id in temporary variable
      $editid = $values->editid;

      // Unset edit id (otherwise nette db refuse to insert values)
      unset($values->editid);

      // Update task with values
      $this->db->table(self::TABLE_TASKS)->wherePrimary($editid)->update($values);

      // Send flash message
      $this->flashMessage("Task successfully updated.", "info");

      // Disable edit mode  ??
      $this->editId = null;

      // If not, use create mode
    } else {
      // Unset edit id (otherwise nette db refuse to insert values)
      unset($values->editid);

      // Insert new task into db
      $this->db->table(self::TABLE_TASKS)->insert($values);

      // Send flash message
      $this->flashMessage("Task successfully added.", "success");
    }

    // Clean form after sending
    if($this->isAjax()) {
        // If ajax, foreward
        $this->forward("resetForm!");
    } else {
      // If not, redirect
      $this->redirect("default");
    }
  }

  public function handleResetForm() {
    // Redraw page
    $this->redrawControl();
  }

  public function renderDefault() {
    // Load all tasks
    $this->template->tasks = $this->db->table(self::TABLE_TASKS)->order(self::COLUMN_STATE);

    // And inform template about edit mode
    $this->template->isEditing = $this->editId != null;
  }

  public function handleDeleteRequest($id) {
    // If task is found
    if($this->db->table(self::TABLE_TASKS)->wherePrimary($id)->count() > 0) {
      // Send task to template
      $this->template->taskToBeDeleted = $this->db->table(self::TABLE_TASKS)->wherePrimary($id)->fetch();

      // Redraw modal
      $this->redrawControl("deleteModal");
    } else {
      // Otherwise, when task is not found, just inform
      $this->flashMessage("Task id is invalid.", "danger");
    }
    // and update flashes
    $this->redrawControl("flashes");
  }

  public function handleDelete($id) {
    // If task is found
    if($this->db->table(self::TABLE_TASKS)->wherePrimary($id)->count() > 0) {
      // Delete it
      $this->db->table(self::TABLE_TASKS)->wherePrimary($id)->delete();

      // Inform user by flash message
      $this->flashMessage("Task removed.", "danger");

      // And redraw list of tasks
      $this->redrawControl("tasks");
    } else {
      // Otherwise, when task is not found, just inform
      $this->flashMessage("Task id is invalid.", "danger");
    }
    // and update flashes
    $this->redrawControl("flashes");
  }

  public function handleEdit($id) {
    // If task is found
    if($this->db->table(self::TABLE_TASKS)->wherePrimary($id)->count() > 0) {
      // Enable edit mode by setting edit id valid int (not null)
      $this->editId = $id;

      // Inform user
      $this->flashMessage("You can now edit task using form.", "warning");

      // Redraw form
      $this->redrawControl("form");
    } else {
      // Otherwise, inform user
      $this->flashMessage("Task id is invalid.", "danger");
    }

    // Update flashes
    $this->redrawControl("flashes");
  }

  public function handleCancelEdit() {
    // Disable edit mode by setting edit id to null
    $this->editId = null;

    // Inform user
    $this->flashMessage("Editing canceled.", "warning");

    // Redraw form
    $this->redrawControl("form");

    // Redraw flashes
    $this->redrawControl("flashes");
  }

  public function handleDone($id) {
    // If task is found
    if($this->db->table(self::TABLE_TASKS)->wherePrimary($id)->count() > 0) {
      // Update it (set state to done)
      $this->db->table(self::TABLE_TASKS)->wherePrimary($id)->update([ self::COLUMN_STATE => self::STATE_DONE ]);

      // Inform user
      $this->flashMessage("Task marked as done.", "success");

      // Redraw list of tasks
      $this->redrawControl("tasks");
    } else {
      // Otherwise inform
      $this->flashMessage("Task id is invalid.", "danger");
    }

    // Redraw flashes
    $this->redrawControl("flashes");
  }

  public function handleRevert($id) {
    // If task is found
    if($this->db->table(self::TABLE_TASKS)->wherePrimary($id)->count() > 0) {
      // Update it (set state to new)
      $this->db->table(self::TABLE_TASKS)->wherePrimary($id)->update([ self::COLUMN_STATE => self::STATE_NEW ]);

      // Inform user
      $this->flashMessage("Reverted marking task as done.", "success");

      // Redraw list of tasks
      $this->redrawControl("tasks");
    } else {
      // Otherwise inform
      $this->flashMessage("Task id is invalid.", "danger");
    }

    // Redraw flashes
    $this->redrawControl("flashes");
  }
}
