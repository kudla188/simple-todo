Simple todo
=================

This is a simple todo list. It can be used with Ajax or without, both ways it works.
I made this using only one presenter, because it's extremely small app and can be easily moved into component later on.
When using non-ajax version, there is one thing that is not implemented - delete confirm dialog.

You can check out working [example](https://todo.senohrabek.net/).


Requirements
------------

PHP 5.6 or higher.
IE 8+?


Installation
------------

This app is working with composer, download it following [the instructions](https://doc.nette.org/composer). Then obtain nette using command:

	composer update

Make directories `temp/` and `log/` writable. Scheme of database can be found under scheme.sql and project should be opened from web-project/www. (in browser)